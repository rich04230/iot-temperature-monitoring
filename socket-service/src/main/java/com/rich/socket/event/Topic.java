package com.rich.socket.event;

public class Topic {

    public static final String IOT_DEVICE = "/topic/iot/device";
    public static final String IOT_DEVICE_ABNORMAL = "/topic/iot/device/abnormal";
}
