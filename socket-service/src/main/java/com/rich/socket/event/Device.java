package com.rich.socket.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Device {

    private long id;
    private String temperature;
    private long timestamp;

    public static Device create() {
        Random r = new Random();
        String temperature = String.format("%03dF",
                r.longs(14, 120).findFirst().getAsLong());

        return new Device(r.longs(1, 10).findFirst().getAsLong(),
                temperature, Instant.now().toEpochMilli());
    }
}
