package com.rich.socket.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MessageListener {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    List<String> recordStorage = new ArrayList<>();

    @KafkaListener(topics = "streaming.iot.device", groupId = "listenIotDevice1", containerFactory = "kafkaIotDeviceListener")
    private void listenIotDevice(Device device) {
        log.debug("Received device: {}", device);
        this.simpMessagingTemplate.convertAndSend(Topic.IOT_DEVICE, device);
    }

    @KafkaListener(topics = "streaming.iot.device.abnormal", groupId = "listenIotDevice12", containerFactory = "kafkaIotDeviceListener")
    private void listenIotAbnormalDevice(Device device) {
        log.debug("Received abnormal device: {}", device);
        this.simpMessagingTemplate.convertAndSend(Topic.IOT_DEVICE_ABNORMAL, device);
    }

    public List<String> getAllRecords() {
        return recordStorage;
    }

}
