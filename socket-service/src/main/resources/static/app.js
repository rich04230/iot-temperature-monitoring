var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/socket-service');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/iot/device', function (device) {
            console.log(device.body)
            showDevice(device.body);
        });
        stompClient.subscribe('/topic/iot/device/abnormal', function (device) {
            console.log(device.body)
            showWarning(device.body);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function showDevice(message) {
    $("#devices").append("<tr><td>" + message + "</td></tr>");
}

function showWarning(message) {
    $("#devices").append("<tr><td><font color='red'>" + message + "</font></td></tr>");
}

function clearMessages(){
    $("#devices").empty();
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#clear" ).click(function() { clearMessages(); });
});

