package com.rich.iot;

import com.rich.iot.model.Device;
import com.rich.iot.service.IotDataSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Slf4j
@Component
@Profile("!test")
public class IotDataRunner implements CommandLineRunner {

    @Autowired
    IotDataSender iotDataSender;

    @Override
    public void run(String... args) {
        Stream.generate(Device::create).forEach(d -> {
            log.info("Device info: {}", d);
            iotDataSender.send(d);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                log.error("Unexpected error", e);
                Thread.currentThread().interrupt();
            }
        });
    }
}
