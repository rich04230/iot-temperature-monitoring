package com.rich.iot.service;

import com.rich.iot.model.Device;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IotDataSender {

    public static final String topic = "streaming.iot.device";

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Qualifier("kafkaJsonTemplate")
    @Autowired
    KafkaTemplate kafkaTemplate;

    public void send(Device device) {
        log.info("send device: {} to topic: {}", device, topic);
        kafkaTemplate.send(topic, device);
    }
}
